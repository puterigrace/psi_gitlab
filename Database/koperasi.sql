CREATE TABLE `t_code` (
  `code_id` INT(1) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`code_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_pelanggan` (
  `id_pelanggan` VARCHAR(32) NOT NULL,
  `email` VARCHAR(32) NOT NULL,
  `status` CHAR(20) DEFAULT NULL,
  `username_pelanggan` VARCHAR(32) NOT NULL,
  `id_rekening` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id_pelanggan`),
  KEY `id_rekening` (`id_rekening`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_pemasukan` (
  `id` VARCHAR(45) NOT NULL,
  `keterangan` VARCHAR(300) DEFAULT NULL,
  `tanggal` VARCHAR(32) NOT NULL,
  `jumlah` VARCHAR(32) NOT NULL,
  `jenis` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_pembelian` (
  `id_pembelian` VARCHAR(45) NOT NULL,
  `tanggal` DATE NOT NULL,
  `keterangan` VARCHAR(300) NOT NULL,
  `total` VARCHAR(300) NOT NULL,
  `saldo` INT(11) NOT NULL,
  `id_rekening` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id_pembelian`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_pengeluaran` (
  `id` VARCHAR(45) NOT NULL,
  `keterangan` VARCHAR(300) NOT NULL,
  `tanggal` VARCHAR(32) NOT NULL,
  `jumlah` VARCHAR(32) NOT NULL,
  `jenis` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_produk` (
  `id_produk` VARCHAR(45) NOT NULL,
  `tanggal_kadaluarsa` DATE NOT NULL,
  `nama_produk` VARCHAR(100) NOT NULL,
  `harga_produk` INT(11) NOT NULL,
  `jenis_produk` VARCHAR(32) NOT NULL,
  `status` INT(11) DEFAULT '1',
  `stok_produk` INT(11) NOT NULL,
  PRIMARY KEY (`id_produk`)
) ENGINE=INNODB DEFAULT CHARSET=latin1



CREATE TABLE `t_rekening` (
  `id_rekening` VARCHAR(32) NOT NULL,
  `pin_rekening` VARCHAR(200) NOT NULL,
  `saldo_rekening` INT(11) NOT NULL,
  `no_rekening` INT(11) NOT NULL,
  PRIMARY KEY (`id_rekening`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_riwayat` (
  `id_riwayat` VARCHAR(45) NOT NULL,
  `tanggal` DATE NOT NULL,
  `keterangan` VARCHAR(300) NOT NULL,
  `total` VARCHAR(300) NOT NULL,
  `saldo` INT(11) NOT NULL,
  `id_rekening` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id_riwayat`),
  KEY `id_rekening` (`id_rekening`),
  CONSTRAINT `t_riwayat_ibfk_1` FOREIGN KEY (`id_rekening`) REFERENCES `t_rekening` (`id_rekening`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_role` (
  `id_role` VARCHAR(32) NOT NULL,
  `nama_role` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_sale` (
  `sale_id` VARCHAR(45) NOT NULL,
  `id_produk` VARCHAR(45) NOT NULL,
  `qty` INT(11) NOT NULL,
  `total` INT(11) NOT NULL,
  `tanggal` DATETIME NOT NULL,
  PRIMARY KEY (`sale_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_save_transaksi` (
  `save_transaksi_id` VARCHAR(45) NOT NULL,
  `nama` VARCHAR(115) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`save_transaksi_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_supplier` (
  `id_supplier` VARCHAR(32) NOT NULL,
  `nama_supplier` CHAR(32) DEFAULT NULL,
  `alamat` VARCHAR(20) DEFAULT NULL,
  `no_rek_supplier` INT(11) DEFAULT NULL,
  `contact` INT(20) DEFAULT NULL,
  PRIMARY KEY (`id_supplier`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_temp_transaksi` (
  `temp_transaksi_id` VARCHAR(45) NOT NULL,
  `code` VARCHAR(45) NOT NULL,
  `id_produk` VARCHAR(45) NOT NULL,
  `qty` INT(11) NOT NULL,
  PRIMARY KEY (`temp_transaksi_id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

CREATE TABLE `t_user` (
  `id_user` VARCHAR(45) NOT NULL,
  `username` VARCHAR(32) NOT NULL,
  `password` VARCHAR(300) NOT NULL,
  `nama_user` CHAR(32) NOT NULL,
  `email_user` VARCHAR(32) NOT NULL,
  `id_role` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`id_user`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `t_user_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `t_role` (`id_role`)
) ENGINE=INNODB DEFAULT CHARSET=latin1

