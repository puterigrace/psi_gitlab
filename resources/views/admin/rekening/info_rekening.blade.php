@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            
@foreach($rekening as $r)
    <form action="/admin/rekening/info" method="get">
		{{ csrf_field() }}
		<div class="form-group">
            <label for="id_rekening">ID Rekening</label>
            <input type="text" class="form-control" name="id_rekening" value="{{$r->id_rekening}}" placeholder="ID Rekening" disabled="">
            <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
       <div class="form-group">
            <label for="saldo_rekening">Saldo Rekening</label>
            <input type="text" class="form-control" name="saldo_rekening" value="Rp.{{ number_format($r->saldo_rekening,0) }}" placeholder="Saldo Rekening" disabled="">
        </div>
@endforeach

@foreach($pelanggan as $p)
        <div class="form-group">
            <label for="username_pelanggan">Nama Pelanggan</label>
            <input type="text" class="form-control" name="username_pelanggan" value="{{$p->username_pelanggan}}" placeholder="Nama Pelanggan" disabled="">
        </div>
        @endforeach
        <button class="btn btn-success btn-isi"><i class="fa fa-fw fa-plus"></i>Isi Saldo</button>
    </form>

    <!--onclick="window.location.href='/admin/rekening'"-->

    </div>

    <div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>Riwayat Transaksi Rekening</h3>
            </div>
            <div class="box-body">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Keterangan</th>
      <th scope="col">Jumlah Mutasi</th>
      <th scope="col">Saldo</th>
      </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp

    @foreach($pembelian as $p)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $p->tanggal }}</td>
      <td>{{ $p->keterangan }}</td>
      <td>Rp.{{ number_format($p->total,0) }}</td>
      <td>Rp.{{ number_format($p->saldo,0) }}</td>
    </tr>
    @endforeach
  </tbody>
</table>


    <link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

    <!-- Modal Isi Saldo -->
<div class="modal modal-default fade" id="modal-tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Isi Saldo</h4>
      </div>
      <div class="modal-body">

      <form role="form" action="{{ url('admin/rekening/inject') }}" method="POST">
                  {{ csrf_field() }}
             @foreach($rekening as $r)
             <input type="hidden" name="keterangan" value="Inject saldo rekening">
             <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail3">ID Rekening</label>
                  <input type="text" class="form-control" name="id_rekening" placeholder="Masukkan ID Rekening Anda" value="{{$r->id_rekening}}" readonly>
                </div>
              </div>
             <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail2">Jumlah Saldo Anda</label>
                  <input type="text" class="form-control" name="saldo" placeholder="Rp" value="{{$r->saldo_rekening}}" readonly>
                </div>
              </div>                          
             @endforeach                    
            <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nominal Saldo Yang Akan Ditambahkan</label>
                  <input type="text" class="form-control" name="nominal" placeholder="Rp">
                </div>
              </div>

              <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail4">PIN Rekening</label>
                    <input type="password" class="form-control" name="pin_rekening" placeholder="Masukkan PIN anda">
              </div>
              </div>              
              <!-- /.box-body -->
              <div class="box-footer" >
                <button type="submit" class="btn btn-success btn-block"><i class="fa fa-fw"></i>Proses</button>
              </div>
             
            </form>

@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var flash = "{{ Session::has('pesan') }}";
		if(flash){
			var pesan = "{{ Session::get('pesan') }}";
			swal('Success',pesan,'success');
		}

		$('.table-barang').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ url('admin/barang/yajra') }}",
	        columns: [
	            {data: 'nama', name: 'nama'},
	            {data: 'harga_awal', name: 'harga_awal'},
	            {data: 'discount', name: 'discount'},
	            {data: 'harga_akhir', name: 'harga_akhir'},
	            {data: 'action', name: 'action', orderable: false, searchable: false}
	        ],
          order: [ [0, 'asc'] ]
	    });

	    // Ketika btn isi di klik
	    $('.btn-isi').click(function(e){
	    	e.preventDefault();
	    	$('#modal-tambah').modal();
	    });

	    // Button edit di klik
	    $('body').on('click','.btn-edit',function(e){
	    	var id = $(this).attr('barang-id');
	    	$.ajax({
	    		'type':'get',
	    		'url':"{{ url('admin/barang/get') }}"+'/'+id,
	    		success: function(data){
	    			console.log(data);
	    			$('#modal-edit').find("input[name='nama']").val(data.nama);
	    			$('#modal-edit').find("input[name='harga_awal']").val(data.harga_awal);
	    			$('#modal-edit').find("input[name='discount']").val(data.discount);

	    			var url = "{{ url('admin/barang') }}"+'/'+id;

	    			$('#modal-edit').find('form').attr('action',url);
	    		}
	    	})

	    	$('#modal-edit').modal();
	    })
	})
</script>

@endsection

