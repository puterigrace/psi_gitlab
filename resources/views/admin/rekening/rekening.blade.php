@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            </div>
            <div class="box-body">
            @if(Session::get('admin'))
<button onclick="window.location.href='/admin/rekening/tambah'" class="btn btn-block btn-primary btn-tambah"><i class="fa fa-fw fa-plus"></i>Tambah</button>
<br/>
@endif
<form class="form-inline" action="/admin/rekening/cari" method="GET">
<input type="text" class="form-control" name="cari" placeholder="ID Rekening"  style="width:200px;" value="{{ old('cari') }}">
  <input class="btn btn-primary btn-search" type="submit" value="Search">		
</form>
<br/>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">ID Rekening</th>
      <th scope="col">Saldo Rekening</th>
      @if(Session::get('admin'))
      <th scope="col">Action</th>
      @endif
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp
    
    @foreach($rekening as $r)
    
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $r->id_rekening }}</td>
      <td>Rp.{{ number_format($r->saldo_rekening,0) }}</td>
      @if(Session::get('admin'))
      <td>
      <button onclick="window.location.href='/admin/rekening/edit/{{$r->id_rekening}}'" class="btn btn-sm btn-success btn-edit"><i class="fa fa-fw fa-pencil"></i>Edit</button>
      |
      <button onclick="window.location.href='/admin/rekening/hapus/{{$r->id_rekening}}'" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-fw fa-trash"></i>Hapus</button>
      |
      <button onclick="window.location.href='/admin/rekening/info/{{$r->id_rekening}}'" class="btn btn-sm btn-primary btn-info"><i class="fa fa-fw fa-paperclip"></i>Info</button>
      </td>
      @endif
    </tr>
    @endforeach
  </tbody>
</table>
@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var flash = "{{ Session::has('sukses') }}";
		if(flash){
			var pesan = "{{ Session::get('sukses') }}";
			swal('Success',pesan,'success');
		}
  
    $(document).ready(function(){
      var flash = "{{Session::has('failed')}}";
      if(flash){
        var failed = "{{Session::get('failed')}}";
        swal('Error',failed,'error');        
      }
    })
  })
 
</script>

@endsection