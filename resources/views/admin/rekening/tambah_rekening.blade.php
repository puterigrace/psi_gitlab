@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            

    <form action="/admin/rekening/store" method="post">
		{{ csrf_field() }}
		<div class="form-group">
            <label for="id_rekening">ID Rekening</label>
            <input type="text" class="form-control" name="id_rekening"  placeholder="ID Rekening" required>
            <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="saldo_rekening">Saldo Rekening</label>
            <input type="text" class="form-control" name="saldo_rekening" placeholder="Saldo Rekening" required>
        </div>
        <div class="form-group">
            <label for="pin_rekening">PIN Rekening</label>
            <input type="password" class="form-control" name="pin_rekening" placeholder="PIN Rekening" required>
        </div>
        <div class="form-group">
            <label for="pin_rekening">Confirm PIN Rekening</label>
            <input type="password" class="form-control" name="confirm_pin" placeholder="Re-enter PIN" required>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
	</form>
    </div>
@endsection

