@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            
@foreach($rekening as $r)
    <form action="/admin/rekening/update" method="post">
		{{ csrf_field() }}
		<div class="form-group">
            <label for="id_rekening">ID Rekening</label>
            <input type="text" class="form-control" name="id" value="{{$r->id_rekening}}" placeholder="ID Rekening" readonly>
            <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="saldo_rekening">Saldo Rekening</label>
            <input type="text" class="form-control" name="saldo_rekening" value="{{$r->saldo_rekening}}" placeholder="Saldo Rekening" readonly>
        </div>
        <div class="form-group">
            <label for="pin_rekening">PIN Rekening</label>
            <input type="password" class="form-control" name="pin_rekening" placeholder="PIN Rekening">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button onclick="window.location.href='/admin/rekening'" class="btn btn-danger">Cancel</button>
    </form>
@endforeach
    </div>
@endsection