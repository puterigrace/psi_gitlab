@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            </div>
            <div class="box-body">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama User</th>
      <th scope="col">Email</th>
      <th scope="col">Username</th>
      <th scope="col">Status</th>      
      <th scope="col">Action</th>   
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp

    @foreach($user as $u)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $u->nama_user }}</td>
      <td>{{ $u->email_user }}</td>
      <td>{{ $u->username }}</td>
      <td>
      @if($u->id_status == 1)
      <button class="btn btn-sm btn-success"><i class="fa fa-fw fa-check"></i>Aktif</button>
      @endif

      @if($u->id_status == 0)
      <button class="btn btn-sm btn-danger"><i class="fa fa-fw fa-remove"></i>Non Aktif</button>
      @endif
      </td>
      <td>
      @if($u->id_status == 0)
      <button onclick="window.location.href='/admin/user/aktivasi/{{$u->id_user}}'" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-edit"></i>Aktifkan</button>
      @endif
      <button onclick="window.location.href='/admin/user/hapus/{{$u->id_user}}'" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i>Hapus</button>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var flash = "{{ Session::has('sukses') }}";
		if(flash){
			var pesan = "{{ Session::get('sukses') }}";
			swal('Success',pesan,'success');
    }
  	$(document).ready(function(){
		var flash = "{{ Session::has('failed') }}";
		if(flash){
			var pesan = "{{ Session::get('failed') }}";
			swal('Error',pesan,'error');
    }  			   
	}) 			   
	})
</script>

@endsection
