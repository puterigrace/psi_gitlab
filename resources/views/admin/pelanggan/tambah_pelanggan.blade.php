@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            

     <form action="/admin/pelanggan/store" method="post">
		{{ csrf_field() }}
		<div class="form-group">
            <label for="id_pelanggan">ID Pelanggan</label>
            <input type="text" class="form-control" name="id_pelanggan"  placeholder="ID Pelanggan" required>
            <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="email">Email Pelanggan</label>
            <input type="text" class="form-control" name="email" placeholder="Email" required>
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" class="form-control" name="status" placeholder="Status" required>
        </div>
        <div class="form-group">
            <label for="username_pelanggan">Nama Pelanggan</label>
            <input type="text" class="form-control" name="username_pelanggan" placeholder="Nama" required>
        </div>
        <div class="form-group">
            <label for="id_rekening">ID Rekening</label>
            <input type="text" class="form-control" name="id_rekening" placeholder="ID Rekening" required>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
	</form>
    </div>
@endsection