@extends('admin.layouts.dashboard')

@section('content')
<form class="form-signin" action="/admin/pelanggan/transaksi" method="GET">
  {{ csrf_field() }}  
  <h1 class="h3 mb-3 font-weight-normal">Masukkan ID Pelanggan Anda</h1>
  <label for="id_pelanggan" class="sr-only">ID Pelanggan</label>
  <input type="text" name="id_pelanggan" class="form-control" placeholder="ID Pelanggan" required> <br/>
  <button class="btn btn-md btn-success" type="submit">Mulai Transaksi</button>  
</form>
@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var flash = "{{ Session::has('sukses') }}";
		if(flash){
			var pesan = "{{ Session::get('sukses') }}";
			swal('Success',pesan,'success');
		}
  })

    $(document).ready(function(){
      var flash = "{{Session::has('failed')}}";
      if(flash){
        var failed = "{{Session::get('failed')}}";
        swal('Error',failed,'error');        
      }
    })
			   

</script>

@endsection