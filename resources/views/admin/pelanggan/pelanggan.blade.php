@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            </div>
            <div class="box-body">
@if(Session::get('admin'))
<button onclick="window.location.href='/admin/pelanggan/tambah'" class="btn btn-block btn-primary btn-tambah"><i class="fa fa-fw fa-plus"></i>Tambah</button>
<br/>
@endif
<form class="form-inline" action="/admin/pelanggan/cari" method="GET">
<input type="text" class="form-control" name="cari" placeholder="Search..."  style="width:200px;" value="{{ old('cari') }}">
  <input class="btn btn-primary btn-search" type="submit" value="Search">		
</form>
<br/>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">ID Pelanggan</th>
      <th scope="col">Nama Pelanggan</th>
      <th scope="col">ID Rekening</th>
      @if(Session::get('admin'))
      <th scope="col">Action</th>
      @endif
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp

    @foreach($pelanggan as $p)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $p->id_pelanggan }}</td>
      <td>{{ $p->username_pelanggan }}</td>
      <td>{{ $p->id_rekening }}</td>
      @if(Session::get('admin'))
      <td>
      <button onclick="window.location.href='/admin/pelanggan/edit/{{$p->id_pelanggan}}'" class="btn btn-sm btn-success btn-edit"><i class="fa fa-fw fa-pencil"></i>Edit</button>
      |
      <button onclick="window.location.href='/admin/pelanggan/hapus/{{$p->id_pelanggan}}'" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-fw fa-trash"></i>Hapus</button>
      </td>
      @endif
    </tr>
    @endforeach
  </tbody>
</table>
@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var flash = "{{ Session::has('sukses') }}";
		if(flash){
			var pesan = "{{ Session::get('sukses') }}";
			swal('Success',pesan,'success');
    }
  	$(document).ready(function(){
		var flash = "{{ Session::has('failed') }}";
		if(flash){
			var pesan = "{{ Session::get('failed') }}";
			swal('Error',pesan,'error');
    }  			   
	}) 			   
	})
</script>

@endsection


