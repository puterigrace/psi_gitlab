@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            
@foreach($pelanggan as $p)
    <form action="/admin/pelanggan/update" method="post">
		{{ csrf_field() }}
		<div class="form-group">
            <label for="id_pelanggan">ID Pelanggan</label>
            <input type="text" class="form-control" name="id" value="{{$p->id_pelanggan}}" placeholder="ID Pelanggan" readonly>
            <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="email">Email Pelanggan</label>
            <input type="text" class="form-control" name="email" value="{{$p->email}}" placeholder="Email"> 
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <input type="text" class="form-control" name="status" value="{{$p->status}}" placeholder="Status">
        </div>
        <div class="form-group">
            <label for="username_pelanggan">Username Pelanggan</label>
            <input type="text" class="form-control" name="username_pelanggan" value="{{$p->username_pelanggan}}" placeholder="Username">
        </div>
        <div class="form-group">
            <label for="id_rekening">ID Rekening</label>
            <input type="text" class="form-control" name="id_rekening" value="{{$p->id_rekening}}" placeholder="ID Rekening" readonly>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
@endforeach
    </div>
@endsection

