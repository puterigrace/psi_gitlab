@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            
@foreach($supplier as $s)
    <form action="/admin/supplier/update" method="post">
		{{ csrf_field() }}
		<div class="form-group">
            <label for="id_supplier">ID Supplier</label>
            <input type="text" class="form-control" name="id" value="{{$s->id_supplier}}"  placeholder="ID Supplier" readonly>
            <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="nama_supplier">Nama Supplier</label>
            <input type="text" class="form-control" name="nama_supplier" value="{{$s->nama_supplier}}" placeholder="Nomor Supplier">
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control" name="alamat" value="{{$s->alamat}}" placeholder="Alamat">
        </div>
        <div class="form-group">
            <label for="no_rek_supplier">Nomor Rekening Supplier</label>
            <input type="text" class="form-control" name="no_rek_supplier" value="{{$s->no_rek_supplier}}" placeholder="Rekening Supplier">
        </div>
        <div class="form-group">
            <label for="contact">Contact</label>
            <input type="text" class="form-control" name="contact" value="{{$s->contact}}" placeholder="Contact">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button onclick="window.location.href='/admin/supplier'" class="btn btn-danger">Cancel</button>
    </form>
@endforeach
    </div>
@endsection