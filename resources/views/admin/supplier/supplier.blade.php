@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            </div>
            <div class="box-body">
            @if(Session::get('admin'))
<button onclick="window.location.href='/admin/supplier/tambah'" class="btn btn-block btn-primary btn-tambah"><i class="fa fa-fw fa-plus"></i>Tambah</button>
@endif
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">ID Supplier</th>
      <th scope="col">Nama Supplier</th>
      <th scope="col">Alamat</th>
      <th scope="col">Nomor Rekening</th>
      <th scope="col">Contact</th>
      @if(Session::get('admin'))
      <th scope="col">Action</th>
      @endif
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp

    @foreach($supplier as $s)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $s->id_supplier }}</td>
      <td>{{ $s->nama_supplier }}</td>
      <td>{{ $s->alamat }}</td>
      <td>{{ $s->no_rek_supplier }}</td>
      <td>{{ $s->contact }}</td>
      @if(Session::get('admin'))
      <td>
      <button onclick="window.location.href='/admin/supplier/edit/{{$s->id_supplier}}'" class="btn btn-sm btn-success btn-edit"><i class="fa fa-fw fa-pencil"></i>Edit</button>
      |
      <button onclick="window.location.href='/admin/supplier/hapus/{{$s->id_supplier}}'" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-fw fa-trash"></i>Hapus</button>
      </td>
      @endif
    </tr>
    @endforeach
  </tbody>
</table>
@endsection



