@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            

    <form action="/admin/supplier/store" method="post">
		{{ csrf_field() }}
		<div class="form-group">
            <label for="id_supplier">ID Supplier</label>
            <input type="text" class="form-control" name="id_supplier"  placeholder="ID Supplier" required>
            <!--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>-->
        </div>
        <div class="form-group">
            <label for="nama_supplier">Nama Supplier</label>
            <input type="text" class="form-control" name="nama_supplier" placeholder="Nama Supplier"required>
        </div>
        <div class="form-group">
            <label for="alamat">Alamat</label>
            <input type="text" class="form-control" name="alamat" placeholder="Alamat"required>
        </div>
        <div class="form-group">
            <label for="no_rek_supplier">Nomor Rekening Supplier</label>
            <input type="text" class="form-control" name="no_rek_supplier" placeholder="Rekening Supplier"required>
        </div>
        <div class="form-group">
            <label for="contact">Contact</label>
            <input type="text" class="form-control" name="contact" placeholder="Contact"required>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
	</form>
    </div>
@endsection