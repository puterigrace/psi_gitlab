@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header">
				<h3>{{ $title }}</h3>
			</div>
			<div class="box-body">
			

      @if(Session::get('admin'))
				<button class="btn btn-block btn-primary btn-tambah"><i class="fa fa-fw fa-cart-plus"></i> Tambah</button>
        @endif
				<table class="table table-stripped table-bordered table-barang">
					<thead>
						<tr>
              <th>ID Produk</th>
							<th>Nama</th>
							<th>Harga Produk</th>
							<th>Jenis Produk</th>
              <th>Stok Produk</th>
              <th>Tanggal Kadaluarsa</th>
              @if(Session::get('admin'))
							<th>Action</th>
              @endif
						</tr>
					</thead>
				</table>



			</div>
		</div>
	</div>
</div>

<!-- Modal Tambah -->
<div class="modal modal-default fade" id="modal-tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Tambah Produk</h4>
      </div>
      <div class="modal-body">
        


      		<form role="form" action="{{ url('admin/produk') }}" method="POST">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">ID Produk</label>
                  <input type="text" name="id_produk" class="form-control" id="exampleInputEmail1" placeholder="ID Produk">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Produk</label>
                  <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Nama Produk">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga Produk</label>
                  <input type="text" name="harga_produk" class="form-control" id="exampleInputEmail1" placeholder="Harga Produk">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Jenis Produk</label>
                  <input type="text" name="jenis_produk" class="form-control" id="exampleInputEmail1" placeholder="Jenis Produk">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Stok Produk</label>
                  <input type="number" name="stok_produk" class="form-control" id="exampleInputEmail1" value="0" min="0">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal Kadaluarsa</label>
                  <input type="date" name="tanggal_kadaluarsa" class="form-control" id="exampleInputEmail1" placeholder="Tanggal Kadaluarsa">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-fw fa-cart-plus"></i> Tambah</button>
              </div>
            </form>



      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@if(Session::get('admin'))
<!-- Modal Edit -->
<div class="modal modal-default fade" id="modal-edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span></button>
        <h4 class="modal-title">Edit Produk</h4>
      </div>
      <div class="modal-body">
        


      		<form role="form" action="#" method="POST">
      			{{ csrf_field() }}
            {{ method_field('PUT') }}
              <div class="box-body">
                  <div class="form-group">
                  <label for="exampleInputID">ID Produk</label>
                  <input type="text" name="id_produk" class="form-control" id="exampleInputID" placeholder="ID Produk">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nama Barang</label>
                  <input type="text" name="nama" class="form-control" id="exampleInputEmail1" placeholder="Nama Barang">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Harga Produk</label>
                  <input type="number" name="harga_produk" class="form-control" id="exampleInputEmail1" placeholder="Harga Produk">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Jenis Produk</label>
                  <input type="text" name="jenis_produk" class="form-control" id="exampleInputEmail1" placeholder="Jenis Produk">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Stok Produk</label>
                  <input type="number" name="stok_produk" class="form-control" id="exampleInputEmail1" value="0">
                </div>
              </div>
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal Kadaluarsa</label>
                  <input type="date" name="tanggal_kadaluarsa" class="form-control" id="exampleInputEmail1" placeholder="Tanggal Kadaluarsa">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-fw fa-cart-plus"></i> Update</button>
              </div>
            </form>



      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
@endif
@endsection

@section('scripts')

<script type="text/javascript">
	$(document).ready(function(){
		var flash = "{{ Session::has('pesan') }}";
		if(flash){
			var pesan = "{{ Session::get('pesan') }}";
			swal('Success',pesan,'success');
		}

		$('.table-barang').DataTable({
	        processing: true,
	        serverSide: true,
	        ajax: "{{ url('admin/produk/yajra') }}",
	        columns: [
              {data: 'id_produk', name:'id_produk'},
	            {data: 'nama_produk', name: 'nama_produk'},
	            {data: 'harga_produk', name: 'harga_produk'},
	            {data: 'jenis_produk', name: 'jenis_produk'},
              {data: 'stok_produk', name: 'stok_produk'},
              {data: 'tanggal_kadaluarsa', name: 'tanggal_kadaluarsa'},
              @if(Session::get('admin'))              
	            {data: 'action', name: 'action', orderable: false, searchable: false}
              @endif
	        ],
          order: [ [0, 'asc'] ]
	    });

	    // Ketika btn tambah di klik
	    $('.btn-tambah').click(function(e){
	    	e.preventDefault();
	    	$('#modal-tambah').modal();
	    });

	    // Button edit di klik
	    $('body').on('click','.btn-edit',function(e){
	    	var id = $(this).attr('barang-id');
	    	$.ajax({
	    		'type':'get',
	    		'url':"{{ url('admin/produk/get') }}"+'/'+id,
	    		success: function(data){
            console.log(data);
            $('#modal-edit').find("input[name='id_produk']").val(data.id_produk);
	    			$('#modal-edit').find("input[name='nama']").val(data.nama);
	    			$('#modal-edit').find("input[name='harga_produk']").val(data.harga_produk);
            $('#modal-edit').find("input[name='jenis_produk']").val(data.jenis_produk);
            $('#modal-edit').find("input[name='stok_produk']").val(data.stok_produk);
            $('#modal-edit').find("input[name='tanggal_kadaluarsa']").val(data.tanggal_kadaluarsa);


	    			var url = "{{ url('admin/produk') }}"+'/'+id;

	    			$('#modal-edit').find('form').attr('action',url);
	    		}
	    	})

	    	$('#modal-edit').modal();
	    })
	})
</script>

@endsection