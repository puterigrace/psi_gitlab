@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            </div>
            <div class="box-body">
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">ID Produk</th>
      <th scope="col">Nama Produk</th>
      <th scope="col">Stok Produk</th>
      <th scope="col">Tanggal Kadaluarsa</th>
      <th scope="col">Status Produk</th>
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    $stok_min=20;
    $tanggal = date('Y-m-d');
    @endphp

    @foreach($barang as $b)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $b->id_produk }}</td>
      <td>{{ $b->nama_produk }}</td>
      <td>{{ $b->stok_produk }}</td>
      <td>{{ $b->tanggal_kadaluarsa }}</td>
      <td>
      @if(($b->stok_produk) < ($stok_min) && $b->stok_produk > 0)
      <button onclick="window.location.href='/admin/supplier'" class="btn btn-md btn-warning "><i class="fa fa-fw fa-warning"></i>Stok Produk Hampir Habis</button>
      @endif
      @if($b->stok_produk == 0)
      <button onclick="window.location.href='/admin/supplier'" class="btn btn-md btn-danger "><i class="fa fa-fw fa-warning"></i>Stok Produk Habis</button>
      @endif
      @if($b->tanggal_kadaluarsa == $tanggal) 
      <button class="btn btn-md btn-danger "><i class="fa fa-fw fa-warning"></i>Produk Sudah Kadaluarsa</button>
      @endif
      @if($b->tanggal_kadaluarsa < $tanggal) 
      <button class="btn btn-md btn-danger "><i class="fa fa-fw fa-warning"></i>Produk Sudah Kadaluarsa</button>
      @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection



