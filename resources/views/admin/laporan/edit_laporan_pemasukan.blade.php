@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            
@foreach($pemasukan as $p)
    <form action="/admin/lapor/update" method="post">
		{{ csrf_field() }}
        <input type="hidden" name="id" value="{{$p->id}}"><br/>
        <input type="hidden" name="jenis" value="Pemasukan">
		<div class="form-group">
            <label for="keterangan">Keterangan</label>
            <input type="text" class="form-control" name="keterangan" rows="3" value="{{$p->keterangan}}" placeholder="Isi Keterangan">
        </div>
        <div class="form-group">
            <label for="tanggal">Tanggal</label>
            <input type="date" class="form-control" name="tanggal" value="{{$p->tanggal}}" placeholder="Tanggal">
        </div>
        <div class="form-group">
            <label for="jumlah">Jumlah Pemasukan</label>
            <input type="text" class="form-control" name="pemasukan" value="{{ $p->jumlah }}" placeholder="Rp">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button onclick="window.location.href='/admin/lapor'" class="btn btn-danger">Cancel</button>
    </form>
@endforeach
    </div>
@endsection