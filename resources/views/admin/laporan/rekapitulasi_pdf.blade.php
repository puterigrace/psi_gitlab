<!DOCTYPE html>
<html>
<head>
	<title>Rekapitulasi</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}
	</style>

<center>
  <h5>Rekapitulasi Pemasukan dan Pengeluaran Koperasi IT Del</h5>
  <hr width="100%" align="center">
</center>

<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Keterangan</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Jumlah</th>
      <th scope="col">Jenis</th>
     
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp

    @foreach($laporan as $l)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $l->keterangan }}</td>
      <td>{{ $l->tanggal }}</td>
      <td>Rp. {{ number_format($l->jumlah,0) }}</td>
      <td>{{ $l->jenis }}</td>    
    </tr>
    @endforeach
  </tbody>
</table>


</body>
</html>



