@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            </div>
            <div class="box-body">
            <a href="/admin/rekapitulasi/cetak_pdf" class="btn btn-primary" target="_blank">CETAK PDF</a>
            <br/>
            <br/>

<form class="form-inline" action="/admin/rekapitulasi/cari" method="GET">
<input type="text" class="form-control" name="cari" placeholder="Bulan"  style="width:200px;" value="{{ old('cari') }}">
  <input class="btn btn-primary btn-search" type="submit" value="Filter">		
</form>
<br/>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Keterangan</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Jumlah</th>
      <th scope="col">Jenis</th>
     
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp

    @foreach($laporan as $l)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $l->keterangan }}</td>
      <td>{{ $l->tanggal }}</td>
      <td>Rp. {{ number_format($l->jumlah,0) }}</td>
      <td>{{ $l->jenis }}</td>    
    </tr>
    @endforeach
  </tbody>
</table>
@endsection



