@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            

    <form action="/admin/laporan/store" method="post">
		{{ csrf_field() }}
        <input type="hidden" name="jenis" value="Pengeluaran">
	    <div class="form-group">
            <label for="keterangan">Keterangan</label>
            <textarea class="form-control" name="keterangan" rows="3" placeholder="Isi Keterangan"></textarea>
        </div>
        <div class="form-group">
            <label for="tanggal">Tanggal</label>
            <input type="date" class="form-control" name="tanggal" placeholder="Tanggal">
        </div>
        <div class="form-group">
            <label for="jumlah">Jumlah Pengeluaran</label>
            <input type="text" class="form-control" name="jumlah" placeholder="Rp">
        </div>
       <button type="submit" class="btn btn-primary">Submit</button>
	</form>
    </div>
@endsection