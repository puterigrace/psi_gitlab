@extends('admin.layouts.dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3>{{$title}}</h3>
            </div>
            <div class="box-body">
            <a href="/admin/laporan/cetak_pdf" class="btn btn-primary" target="_blank">CETAK PDF</a>
            <br/>
            <br/>
<button onclick="window.location.href='/admin/laporan/tambah'" class="btn btn-block btn-primary btn-tambah"><i class="fa fa-fw fa-list"></i>Tambah</button>
<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Keterangan</th>
      <th scope="col">Tanggal</th>
      <th scope="col">Jumlah Pengeluaran</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @php
    $i=1;
    @endphp

    @foreach($pengeluaran as $p)
    <tr>
      <td>{{ $i++ }}</td>
      <td>{{ $p->keterangan }}</td>
      <td>{{ $p->tanggal }}</td>
      <td>Rp. {{ number_format($p->jumlah,0) }}</td>
      <td>
      <button onclick="window.location.href='/admin/laporan/edit/{{$p->id}}'" class="btn btn-sm btn-success btn-edit"><i class="fa fa-fw fa-pencil"></i>Edit</button>
      |
      <button onclick="window.location.href='/admin/laporan/hapus/{{$p->id}}'" class="btn btn-sm btn-danger btn-hapus"><i class="fa fa-fw fa-trash"></i>Hapus</button>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
@endsection



