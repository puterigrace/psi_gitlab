
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="{{ (Request::path() == '/admin') ? 'active' : '' }}">
          <a href="{{url('/admin')}}">
            <i class="fa fa-home"></i> <span>Home</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="{{(Request::path() == 'admin/pelanggan')? 'active' : '' }}">
          <a href="{{url('admin/pelanggan')}}">
            <i class="fa fa-user"></i> <span>Pelanggan</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        @if(Session::get('admin'))
        <li class="{{(Request::path() == 'admin/user')? 'active' : '' }}">
          <a href="{{url('admin/user')}}">
            <i class="fa fa-cog"></i> <span>User</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        @endif

        <li class="{{ (Request::path() == 'admin/pelanggan/login') ? 'active' : '' }}"> <!-- Path sebelumnya "admin/transaksi" -->
          <a href="{{url('admin/pelanggan/login')}}">
            <i class="fa fa-check"></i> <span>Transaksi</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="{{ (Request::path() == 'admin/transaksi') ? 'active' : ''}}">
          <a href="{{url('admin/transaksi')}}">
            <i class="fa fa-th-list"></i> <span>Riwayat Transaksi</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-fire"></i> <span>Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{ (Request::path() == 'admin/produk') ? 'active' : '' }}"><a href="{{url('admin/produk')}}"><i class="fa fa-circle-o"></i> List Produk</a></li>
            <li class="{{ (Request::path() == 'admin/produk/status') ? 'active' : '' }}"><a href="{{url('admin/produk/status')}}"><i class="fa fa-tag"></i> Status Produk</a></li>
          </ul>
         
        </li>
        
        <li class="{{ (Request::path() == 'admin/rekening') ? 'active' : '' }}">
          <a href="{{url('admin/rekening')}}">
            <i class="fa fa-book"></i> <span>Rekening</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="{{ (Request::path() == 'admin/supplier') ? 'active' : '' }}">
          <a href="{{url('admin/supplier')}}">
            <i class="fa fa-refresh"></i> <span>Supplier</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-list-alt"></i> <span>Laporan</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
            <li class="{{ (Request::path() == 'admin/laporan') ? 'active' : '' }}"><a href="{{url('admin/laporan')}}"><i class="fa fa-arrow-up"></i> Laporan Pengeluaran</a></li>
            <li class="{{ (Request::path() == 'admin/lapor') ? 'active' : '' }}"><a href="{{url('admin/lapor')}}"><i class="fa fa-arrow-down"></i> Laporan Pemasukan</a></li>
            <li class="{{ (Request::path() == 'admin/rekapitulasi') ? 'active' : '' }}"><a href="{{url('admin/rekapitulasi')}}"><i class="fa fa-list"></i> Rekapitulasi</a></li>
          </ul>         
        </li>

        <li class="treeview">
        <a href="#">
        <i class="fa fa-wrench"></i> <span>Setting</span>
            <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
            <li class="{{ (Request::path() == 'admin/setting') ? 'active' : '' }}"><a href="{{url('admin/setting')}}"><i class="fa fa-cog"></i> Change Password</a></li>
            </ul>
        </li>

        <li class="{{ (Request::path() == 'keluar') ? 'active' : '' }}">
          <a href="{{url('keluar')}}">
            <i class="fa fa-fw fa-close"></i> <span>Log Out</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>