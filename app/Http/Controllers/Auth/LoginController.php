<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use Hash;
use Uuid;
use Datatables;

class LoginController extends Controller
{
    public function index(){
        return view('auth.login');
    }

    public function admin(){
        return redirect('/admin');
    }

    public function AuthCheck(Request $request) 
    {
        $this->validate($request,
 
            ['username'=>'required'],
 
            ['password'=>'required']
            
        );
 
        $user = $request->input('username');
        $pass = $request->input('password');        
 
        $users = DB::table('t_user')->where(['username'=> $user])->first();
     
               
                if($users->username == $user AND  Hash::check($pass, $users->password) AND $users->id_role == 1 AND $users->id_status == 1){
                    
                    Session::put('admin','Anda login sebagai admin');
                   return redirect('/admin');
        
                } elseif($users->id_role ==2 AND $users->id_status == 1) {
                    Session::put('kasir','Anda login sebagai kasir');  
                   return redirect('/admin');
               
                }else{
                    return redirect('/login');
                }

    }
          
}