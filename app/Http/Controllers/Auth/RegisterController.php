<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Uuid;
use Datatables;
use Hash;

class RegisterController extends Controller
{
    public function index()
    {
        return view('auth.register');
    }

    public function RegisterUser(Request $request)
    {
        $this->validate($request,[
            'username' => 'required',
            'password' => 'required',
            'role' => 'required',
            'email' => 'required',
            'password_confirmation' => 'required',
            'nama' => 'required'
        ]);
        
        $username = $request->username;
        $password = Hash::make($request['password']);
        $role = $request->role;
        $email = $request->email;
        $passwordconfirm = $request->password_confirmation;
        $id_role = "";
        $nama = $request->nama;
        $id = Uuid::generate(4);

        if($role == "Admin"){
            $id_role = 1;
        } elseif($role == "Kasir"){
            $id_role = 2;
        }
            //insert data user baru
        DB::table('t_user')->insert([
            'id_user' => $id,
            'username' => $username,
            'password' => $password,
            'id_role' => $id_role,
            'email_user' => $email,
            'nama_user' => $nama,
            'id_status' => 0
        ]);
        return redirect('/login');
    }
}