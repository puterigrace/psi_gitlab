<?php

namespace App\Http\Controllers\Beranda;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Datatables;
use DB;
use Uuid;
use Session;
use Hash;

class Beranda_controller extends Controller
{
    public function index(){
    	$code = \DB::table('t_code')->where('code_id',1)->value('code');
    	if($code == '0'){
    		$code = Uuid::generate(4);
    	}

    	return view('home',compact('code'));
    }

    public function get($id){
    	$barang = \DB::table('t_produk')->where('id_produk',$id)->first();

    	return response()->json([
    		'id_produk'=>$barang->id_produk,
    		'nama_produk'=>$barang->nama_produk,
			'harga_produk'=>$barang->harga_produk,
			'stok_produk'=>$barang->stok_produk,
    		//'discount'=>$barang->discount,
    		//'harga_akhir'=>$barang->harga_akhir
    	]);
    }

    public function submit(Request $request, $code){
    	$id_produk = $request->id_produk;
		$qty = $request->qty;
		$stok_produk = $request->stok_produk;
		
		//Deklarasi variabel pengurangan jumlah produk
		$stok = \DB::table('t_produk')->where('id_produk',$id_produk)->value('stok_produk');
		$stok_produk = $stok - $qty;
    	\DB::table('t_code')->where('code_id',1)->update([
    		'code'=>$code,
		]);

		if($qty<=$stok){
		//Update jumlah stok produk
		\DB::table('t_produk')->where('id_produk',$id_produk)->update([
			'stok_produk'=>$stok_produk,
		]);
		
    	$cek = count(\DB::table('t_temp_transaksi')->where('id_produk',$id_produk)->where('code',$code)->get());
    	if($cek > 0){
    		$qtyNow = \DB::table('t_temp_transaksi')->where('id_produk',$id_produk)->where('code',$code)->value('qty');
    		\DB::table('t_temp_transaksi')->where('id_produk',$id_produk)->where('code',$code)->update([
    			'qty'=>$qtyNow+$qty
    		]);
    	}else{
    		\DB::table('t_temp_transaksi')->insert([
	    		'temp_transaksi_id'=>Uuid::generate(4),
	    		'code'=>$code,
	    		'id_produk'=>$id_produk,
	    		'qty'=>$qty
	    	]);
		}
	}else{
		Session::flash('failed','Stok produk tidak mencukupi !');
		return redirect('/admin/pelanggan/transaksi');
	}

    	return redirect('/admin/pelanggan/transaksi');
    }

    public function hapus_temp($id,$code){
		//Get data dari database untuk deklarasi variabel
		$id1=\DB::table('t_temp_transaksi')->where('temp_transaksi_id',$id)->where('code',$code)->value('id_produk');
		$stok_p=\DB::table('t_produk')->where('id_produk',$id1)->value('stok_produk');
		$qt=\DB::table('t_temp_transaksi')->where('temp_transaksi_id',$id)->where('code',$code)->value('qty');
		
		//Penambahan jumlah stok kembali
		$stok_pr = $stok_p + $qt;

		//Update jumlah produk jika barang tidak jadi di hapus
		\DB::table('t_produk')->where('id_produk',$id1)->update([
			'stok_produk'=>$stok_pr,
		]);
		
		\DB::table('t_temp_transaksi')->where('temp_transaksi_id',$id)->where('code',$code)->delete();
		
    	return redirect('/admin/pelanggan/transaksi');
    }

    public function selesai(Request $request,$code, $total){
	 	//$total = $request->total;
    	$bayar = $request->bayar;
		$data = \DB::table('t_temp_transaksi')->where('code',$code)->get();
		$id = $request->id;

		//untuk proses autentikasi pin rekening
		$pin = $request->PIN;
		$id = $request->id;
		$rekening = \DB::table('t_rekening')->where(['id_rekening'=>$id])->first();
	
		//jika data rekening dan pin rekening sesuai, input pembelian ke tabel pembelian dan tabel sale dan mengurangi saldo rekening pelanggan
		if($rekening->id_rekening==$id AND Hash::check($pin,$rekening->pin_rekening))
		{
   	
		//untuk proses pengurangan saldo
		$id = $request->id;
		$saldo = \DB::table('t_rekening')->where('id_rekening',$id)->value('saldo_rekening');

		if($total < $saldo){
			$saldo_akhir = $saldo - $total;
			
			\DB::table('t_code')->where('code_id',1)->update([
				'code'=>0
			]);
	
			\DB::table('t_temp_transaksi')->where('code',$code)->delete();
			\DB::table('t_save_transaksi')->where('code',$code)->delete();
			
						
		//update jumlah saldo pelanggan
		\DB::table('t_rekening')->where('id_rekening',$id)->update([
			'saldo_rekening'=>$saldo_akhir
			]);

		//insert tabel pembelian
		\DB::table('t_pembelian')->insert([
    		'id_pembelian'=>Uuid::generate(4),
			'total'=>$total,
			'keterangan'=>"Transaksi pembelian",
			'tanggal'=>date("Y-m-d"),
			'saldo'=>$saldo_akhir,
			'id_rekening'=>$id
			]);
			
		//insert data ke tabel t_sale
		foreach ($data as $key => $dt) {
			\DB::table('t_sale')->insert([
			'sale_id'=>Uuid::generate(4),
			'id_produk'=>$dt->id_produk,
			'qty'=>$dt->qty,
			'total'=>$total,
			'tanggal'=>date("Y-m-d H:i:s"),					
			]);
			}

			Session::flash('sukses', 'Transaksi berhasil !');
			Session::pull('admin'); 
			Session::pull('kasir');
			return redirect('/admin/pelanggan/login');
		
		//jika PIN rekening yang dimasukkan tidak sesuai

		} else {
			Session::flash('failed','Saldo anda tidak mencukupi !');
			return redirect('/admin/pelanggan/login');
		}
		} else {
			Session::flash('failed','Transaksi gagal ! PIN yang anda masukkan salah.');
			return redirect('/admin/pelanggan/login');
		}
    }

    public function hapus_transaksi($code){
    	\DB::table('t_temp_transaksi')->where('code',$code)->delete();
        \DB::table('t_code')->where('code_id',1)->update([
            'code'=>0
        ]);
        \DB::table('t_save_transaksi')->where('code',$code)->delete();

        return redirect('/admin/pelanggan/transaksi');
    }

    public function simpan_transaksi(Request $request, $code){
        \DB::table('t_save_transaksi')->insert([
            'save_transaksi_id'=>Uuid::generate(4),
            'code'=>$code,
            'nama'=>$request->nama
        ]);

        DB::table('t_code')->where('code_id',1)->update([
            'code'=>0
        ]);

        Session::flash('pesan','Transaksi Tersimpan');

        return redirect('/');
    }

    public function open_transaksi($code){
        \DB::table('t_code')->where('code_id',1)->update([
            'code'=>$code
        ]);

        return redirect('/admin/pelanggan/transaksi');
    }

    public function new_transaction($code){
        $cek = count(\DB::table('t_save_transaksi')->where('code',$code)->get());
        if($cek < 1){
            \DB::table('t_temp_transaksi')->where('code',$code)->delete();
        }
        \DB::table('t_code')->where('code_id',1)->update([
            'code'=>0
        ]);

        return redirect('/admin/pelanggan/transaksi');
    }
	//Tampil di kolom produk di halaman kasir
    public function yajra(Request $request){
    	DB::statement(DB::raw('set @rownum=0'));
        $barang = DB::table('t_produk')->select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            	'id_produk',
				'nama_produk',
				'stok_produk',
        	]);
        $datatables = Datatables::of($barang)->editColumn('nama_produk',function($barang){
        	$id = $barang->id_produk;
        	$gambar = asset('loading.gif');
        	return '<span barang-id="'.$id.'" style="cursor:pointer;" class="btn-barang">'.$barang->nama_produk.'</span>'.'<img src="'.$gambar.'" style="display:none;" class="loading">';
        })->rawColumns(['nama_produk']);

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('rownum', 'whereRaw', '@rownum  + 1 like ?', ["%{$keyword}%"]);
        }

        return $datatables->make(true);
    }

    public function keluar(){
		Session::pull('admin'); 
		Session::pull('kasir');
		
		//untuk melakukan destroy pada session tertentu, bisa juga menggunakan Session::forget('nama_session);
		return redirect('/login');
    }
}
