<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

//use DB;
//use Session;
use Uuid;
use Datatables;

class Login_pelanggan_controller extends Controller
{
    public function index(){
    	$title = 'Login Pelanggan';

    	return view('admin.pelanggan.login_pelanggan',compact('title'));
    }
    
    public function AuthCheck(Request $request)
    {
        $this->validate($request,
            ['id_pelanggan'=>'required']                      
        );
        
        $user = $request->input('id_pelanggan'); 
             
        //Jika id_pelanggan sesuai atau tidak sesuai dengan hasil request
        
        $users = DB::table('t_pelanggan')->where(['id_pelanggan'=> $user])->first();
      
        $code = \DB::table('t_code')->where('code_id',1)->value('code');
    	  if($code == '0'){
    		$code = Uuid::generate(4);
    	  }

                if($users->id_pelanggan == $user ){                   
                  Session::put('login','Selamat anda berhasil login');

                  //get data dari database sesuai id pelanggan
                  $pelanggan = \DB::table('t_pelanggan')->where(['id_pelanggan' => $user])->get();
                  $id_rekening = \DB::table('t_pelanggan')->where(['id_pelanggan'=> $user])->value('id_rekening');
                  $rekening = \DB::table('t_rekening')->where(['id_rekening' => $id_rekening])->get();                  
                  //passing data ke view transaksi
                  return view('welcome',['pelanggan'=>$pelanggan,'rekening'=>$rekening],compact('code'));

                  //jika id pelanggan tidak ditemukan atau belum terdaftar
                } else {
                  return redirect('/admin/pelanggan/login');          
                }

  }

    public function logout()
    {
       Session::pull('login'); 
       //untuk melakukan destroy pada session tertentu, bisa juga menggunakan Session::forget('nama_session);
       return redirect('/admin/pelanggan/login');
    }
   
}
