<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Mail\AdminEmail;
use Illuminate\Support\Facades\Mail;
use Session;

class 0User_controller extends Controller
{
    public function index()
    {   
        //mengambil data pelanggan dari database
        $user = DB::table('t_user')->get();
        
        //mengirim data pelanggan ke view
        $title='Daftar User Koperasi IT Del';
        return view('admin.user.user',['user'=>$user],compact('title'));    
    }

    public function AktivasiUser($id)
    {
        //mengambil data berdasarkan id pelanggan
        $email = DB::table('t_user')->where('id_user',$id)->value('email_user');
        $user = DB::table('t_user')->where('id_user',$id)->update([
            'id_status'=>1
        ]);
        Mail::to("$email")->send(new AdminEmail());
        //alihkan halaman
        Session::flash('sukses','User berhasil di-aktivasi !');
        return redirect('/admin/user');
    }

     public function HapusUser($id)
    {
        //mengambil data berdasarkan id pelanggan
        $user = DB::table('t_user')->where('id_user',$id)->delete();
        //alihkan halaman
        Session::flash('sukses','Data pelanggan berhasil di hapus !');
        return redirect('/admin/user');
    }

}