<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Uuid;
use PDF;

class Laporan_pemasukan_controller extends Controller
{
    public function index(){
        $pemasukan = DB::table('t_pemasukan')->get();
    	$title = 'Pemasukan Koperasi IT Del';
    	
    	return view('admin.laporan.laporan_pemasukan_index',['pemasukan'=>$pemasukan],compact('title'));
    }

    public function CetakPdf()
    {
        $title="Laporan Pemasukan";
        $pemasukan = DB::table('t_pemasukan')->get();
        $pdf = PDF::loadview('admin.laporan.laporan_pemasukan_index_pdf',['pemasukan'=>$pemasukan],compact('title'));
        return $pdf->stream();
    }

    public function TambahLaporan()
    {
        $title="Tambah Laporan Pemasukan";
        //view untuk manambah
        return view('admin.laporan.tambah_laporan_pemasukan',compact('title'));       
    }

    public function SimpanLaporan(Request $request)
    {
        $id = Uuid::generate(4);
        $keterangan = $request->keterangan;
        $tanggal = date('Y-m-d');
        $pemasukan = $request->pemasukan;
        $jenis = $request->jenis;

        //insert data pengeluaran baru
        DB::table('t_pemasukan')->insert([
            'id' => $id,
            'keterangan' => $keterangan,
            'tanggal' => $tanggal,
            'jumlah' => $pemasukan,
            'jenis' => $jenis
        ]);

        return redirect('/admin/lapor');
    }

    public function UpdateLaporan(Request $request)
    {
        DB::table('t_pemasukan')->where('id',$request->id)->update([
            'keterangan' =>$request->keterangan,
            'tanggal' => $request->tanggal,
            'jumlah' => $request->pemasukan           
        ]);

        return redirect('/admin/lapor');
    }

    public function EditLaporan($id)
    {
        //mengambil data berdasarkan id supplier
        $pemasukan = DB::table('t_pemasukan')->where('id',$id)->get();
        //passing data ke view
        $title="Edit Laporan Pemasukan";
        return view('admin.laporan.edit_laporan_pemasukan',['pemasukan'=>$pemasukan],compact('title'));
    }

    public function HapusLaporan($id)
    {
        //mengambil data berdasarkan id supplier
        $pemasukan = DB::table('t_pemasukan')->where('id',$id)->delete();
        //alihkan halaman
        return redirect('/admin/lapor');
    }
}
