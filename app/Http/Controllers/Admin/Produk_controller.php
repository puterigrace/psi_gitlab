<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Datatables;
use DB;
use Illuminate\Support\Facades\Input;
use Uuid;
use Session;

class Produk_controller extends Controller
{
    public function index(){
    	$title = 'Daftar Produk Koperasi IT Del';

    	return view('admin.barang.barang_index',compact('title','barang'));
    }

    public function store(Request $request){
        $id_produk = $request->id_produk;
        $nama = $request->nama;
        $harga_produk = $request->harga_produk;
        $jenis_produk = $request->jenis_produk;
        $stok_produk = $request->stok_produk;
        $tanggal_kadaluarsa = $request->tanggal_kadaluarsa;
        
        //$created_at = date("Y-m-d H:i:s");
        //$updated_at = date("Y-m-d H:i:s");

        \DB::table('t_produk')->insert([
            'id_produk'=>$id_produk,
            'nama_produk'=>$nama,
            'harga_produk'=>$harga_produk,
            'jenis_produk'=>$jenis_produk,
            'stok_produk'=>$stok_produk,
            'tanggal_kadaluarsa'=>$tanggal_kadaluarsa
        ]);

        \Session::flash('pesan','Data berhasil ditambahkan');
        return redirect('admin/produk');
    }

    public function update(Request $request,$id){
        $nama = $request->nama;
        $harga_produk = $request->harga_produk;
        $jenis_produk = $request->jenis_produk;
        $stok_produk = $request->stok_produk;
        $tanggal_kadaluarsa = $request->tanggal_kadaluarsa;
        //$harga_akhir = ($harga_awal) - (($discount / 100) * $harga_awal);
        

        \DB::table('t_produk')->where('id_produk',$id)->update([
            'nama_produk'=>$nama,
            'harga_produk'=>$harga_produk,
            'jenis_produk'=>$jenis_produk,
            'stok_produk' =>$stok_produk,
            'tanggal_kadaluarsa' =>$tanggal_kadaluarsa
           
        ]);

        \Session::flash('pesan','Data berhasil di update');
        return redirect('admin/produk');
    }

    public function getData($id){
        $barang = \DB::table('t_produk')->where('id_produk',$id)->first();

        return response()->json([
            'id_produk'=>$barang->id_produk,
            'nama'=>$barang->nama_produk,
            'harga_produk'=>$barang->harga_produk,
            'jenis_produk'=>$barang->jenis_produk,
            'stok_produk'=>$barang->stok_produk,
            'tanggal_kadaluarsa'=>$barang->tanggal_kadaluarsa,
        ]);
    }

    public function yajra(Request $request){
    	$barang = DB::table('t_produk')->select([
    		'id_produk',
    		'nama_produk', 
    		'jenis_produk',
    		'harga_produk',
            'stok_produk',
            'tanggal_kadaluarsa',
    ]);
                    
        return Datatables::of($barang)
            ->addColumn('action', function ($barang) {
                $id = $barang->id_produk;
                return '<a href="#" barang-id="'.$id.'" class="btn btn-xs btn-primary btn-edit"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })->editColumn('harga_produk',function($harga){
                $harga = $harga->harga_produk;
                return 'Rp. '.number_format($harga,0);
            })
              //->editColumn('harga_akhir',function($e){
                //$hrg = $e->harga_akhir;
                //return 'Rp. '.number_format($hrg,0);
            //})
            ->make(true);
	}
}
