<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Uuid;
use PDF;

class Laporan_controller extends Controller
{
    public function index(){
        $pengeluaran = DB::table('t_pengeluaran')->get();
    	$title = 'Pengeluaran Koperasi IT Del';
    	
    	return view('admin.laporan.laporan_index',['pengeluaran'=>$pengeluaran],compact('title'));
    }

    public function CetakPdf()
    {
        $title="Laporan Pengeluaran";
        $pengeluaran = DB::table('t_pengeluaran')->get();
        $pdf = PDF::loadview('admin.laporan.laporan_index_pdf',['pengeluaran'=>$pengeluaran],compact('title'));
        return $pdf->stream();
    }
    
    public function TambahLaporan()
    {
        $title="Tambah Laporan Pengeluaran";
        //view untuk manambah
        return view('admin.laporan.tambah_laporan',compact('title'));       
    }

    public function SimpanLaporan(Request $request)
    {
        $id = Uuid::generate(4);
        $keterangan = $request->keterangan;
        $tanggal = $request->tanggal;
        $jumlah = $request->jumlah;
        $jenis = $request->jenis;

        //insert data pengeluaran baru
        DB::table('t_pengeluaran')->insert([
            'id' => $id,
            'keterangan' => $keterangan,
            'tanggal' => $tanggal,
            'jumlah' => $jumlah,
            'jenis' => $jenis
        ]);

        return redirect('/admin/laporan');
    }

    public function UpdateLaporan(Request $request)
    {
        DB::table('t_pengeluaran')->where('id',$request->id)->update([
            'keterangan' =>$request->keterangan,
            'tanggal' => $request->tanggal,
            'jumlah' => $request->jumlah            
        ]);

        return redirect('/admin/laporan');
    }

    public function EditLaporan($id)
    {
        //mengambil data berdasarkan id supplier
        $pengeluaran = DB::table('t_pengeluaran')->where('id',$id)->get();
        //passing data ke view
        $title="Edit Laporan Pengeluaran";
        return view('admin.laporan.edit_laporan',['pengeluaran'=>$pengeluaran],compact('title'));
    }

    public function HapusLaporan($id)
    {
        //mengambil data berdasarkan id supplier
        $pengeluaran = DB::table('t_pengeluaran')->where('id',$id)->delete();
        //alihkan halaman
        return redirect('/admin/laporan');
    }
}
