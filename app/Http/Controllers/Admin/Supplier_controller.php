<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class Supplier_controller extends Controller
{
    public function index()
    {   
        //mengambil data pelanggan dari database
        $supplier = DB::table('t_supplier')->get();
        
        //mengirim data pelanggan ke view
        $title='Daftar Supplier';
        return view('admin.supplier.supplier',['supplier'=>$supplier],compact('title'));    
    }

    public function TambahSupplier()
    {
        $title="Tambah Supplier";
        //view untuk manambah
        return view('admin.supplier.tambah_supplier',compact('title'));       
    }

    public function SimpanSupplier(Request $request)
    {
        //insert data supplier baru
        DB::table('t_supplier')->insert([
            'id_supplier' => $request->id_supplier,
            'nama_supplier' => $request->nama_supplier,
            'alamat' => $request->alamat,
            'no_rek_supplier' => $request->no_rek_supplier,
            'contact' => $request->contact
        ]);

        return redirect('/admin/supplier');
    }

    public function UpdateSupplier(Request $request)
    {
        DB::table('t_supplier')->where('id_supplier',$request->id)->update([
            'id_supplier' => $request->id,
            'nama_supplier' => $request->nama_supplier,
            'alamat' => $request->alamat,
            'no_rek_supplier' => $request->no_rek_supplier,
            'contact' => $request->contact
        ]);

        return redirect('/admin/supplier');
    }

    public function EditSupplier($id)
    {
        //mengambil data berdasarkan id supplier
        $supplier = DB::table('t_supplier')->where('id_supplier',$id)->get();
        //passing data ke view
        $title="Edit Supplier";
        return view('admin.supplier.edit_supplier',['supplier'=>$supplier],compact('title'));
    }

    public function HapusSupplier($id)
    {
        //mengambil data berdasarkan id supplier
        $supplier = DB::table('t_supplier')->where('id_supplier',$id)->delete();
        //alihkan halaman
        return redirect('/admin/supplier');
    }
}