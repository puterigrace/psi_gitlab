<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
Use Uuid;

class Rekening_controller extends Controller
{
    public function index()
    {   
        //mengambil data pelanggan dari database
        $rekening = DB::table('t_rekening')->get();
        
        //mengirim data pelanggan ke view
        $title='Rekening Pelanggan';
        return view('admin.rekening.rekening',['rekening'=>$rekening],compact('title'));    
    }

    public function TambahRekening()
    {
        $title="Tambah Rekening";
        //view untuk manambah
        return view('admin.rekening.tambah_rekening',compact('title'));       
    }

    public function SimpanRekening(Request $request)
    {
        $confirm_pin = $request->confirm_pin;
        $pin_rekening = $request->pin_rekening;

        if($confirm_pin == $pin_rekening){
        //insert data rekening baru
        DB::table('t_rekening')->insert([
            'id_rekening' => $request->id_rekening,
            'pin_rekening' => Hash::make($request['pin_rekening']),
            'saldo_rekening' =>$request->saldo_rekening           
        ]);
        
        Session::flash('sukses','Registrasi rekening berhasil !');
        return redirect('/admin/rekening');
        }else{
            Session::flash('failed','PIN does not match !');
            return redirect('/admin/rekening');
        }
    
    }

    public function UpdateRekening(Request $request)
    {
        DB::table('t_rekening')->where('id_rekening',$request->id)->update([
            'id_rekening' => $request->id,
            'pin_rekening' => Hash::make($request['pin_rekening']),
            'saldo_rekening' => $request->saldo_rekening
           
        ]);
        
        return redirect('/admin/rekening');
    }

    public function EditRekening($id)
    {
        //mengambil data berdasarkan id rekening
        $rekening = DB::table('t_rekening')->where('id_rekening',$id)->get();
        //passing data ke view
        $title="Edit Rekening";
        return view('admin.rekening.edit_rekening',['rekening'=>$rekening],compact('title'));
    }

    public function HapusRekening($id)
    {
        //mengambil data berdasarkan id rekening
        $rekening = DB::table('t_rekening')->where('id_rekening',$id)->delete();
        //alihkan halaman
        Session::flash('sukses','Rekening telah dihapus !');
        return redirect('/admin/rekening');
    }

    public function InfoRekening($id)
    {
        //mengambil data berdasarkan id rekening
        $rekening = DB::table('t_rekening')->where('id_rekening',$id)->get();
        $pelanggan = DB::table('t_pelanggan')->where('id_rekening',$id)->get();
        
        //mengambil riwayat penggunaan rekening
        $riwayat = DB::table('t_riwayat')->where('id_rekening',$id)->select(
            "t_riwayat.tanggal",
            "t_riwayat.keterangan",
            "t_riwayat.total",
            "t_riwayat.saldo"
        );

        $pembelian = DB::table('t_pembelian')->where('id_rekening',$id)->select(
            "t_pembelian.tanggal",
            "t_pembelian.keterangan",
            "t_pembelian.total",
            "t_pembelian.saldo"
        )
        ->union($riwayat)
        ->get();
        
        //passing data ke view
        $title="Informasi Rekening";
        return view('admin.rekening.info_rekening',['rekening'=>$rekening,'pelanggan'=>$pelanggan,'pembelian'=>$pembelian],compact('title'));

    }
    
    public function TambahSaldo(Request $request){
        $total = $request->nominal;
        $saldo = $request->saldo;
        $id = $request->id_rekening;
        $pin = $request->pin_rekening;
        $saldo1 = ($saldo) + ($total);
        $keterangan = $request->keterangan;
       
        $rekening = DB::table('t_rekening')->where(['id_rekening'=>$id])->first();
        
        if($rekening->id_rekening==$id AND Hash::check($pin,$rekening->pin_rekening))
        {       
        \DB::table('t_rekening')->where([
            ['id_rekening',$id],
        ])->update(['saldo_rekening'=>$saldo1]);

        \DB::table('t_riwayat')->where([
            ['id_rekening',$id],
        ])->insert([
            'id_riwayat'=>Uuid::generate(4),
            'tanggal'=>date('Y-m-d'),
            'keterangan'=>$keterangan,
            'id_rekening'=>$id,
            'total'=>$total,
            'saldo'=>$saldo1
        ]);

        Session::flash('pesan','Saldo Berhasil Ditambahkan');
        return redirect('admin/rekening');

        } else {
            Session::flash('failed','Pengisian Saldo Gagal');
            return redirect('admin/rekening');
        }      
    }

    public function CariRekening(Request $request)
    {
        $title='Rekening Pelanggan';
        
        //menangkap data pencarian
        $cari = $request->cari;

        //mengambil data rekening sesuai pencarian
        $rekening = DB::table('t_rekening')
        ->where('id_rekening','like',"%".$cari."%")
        ->paginate();

        //passing data hasil pencarian
        return view('admin.rekening.rekening',['rekening'=>$rekening],compact('title'));
    }
}