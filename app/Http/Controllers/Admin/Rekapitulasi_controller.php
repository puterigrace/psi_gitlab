<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Uuid;
use PDF;

class Rekapitulasi_controller extends Controller
{
    public function index(){
        $pengeluaran = DB::table('t_pengeluaran')
            ->select(
                "t_pengeluaran.keterangan",
                "t_pengeluaran.tanggal",
                "t_pengeluaran.jumlah",
                "t_pengeluaran.jenis"
            );

        $laporan = DB::table('t_pemasukan')
            ->select(
                "t_pemasukan.keterangan",
                "t_pemasukan.tanggal",
                "t_pemasukan.jumlah",
                "t_pemasukan.jenis"            
                )
            ->union($pengeluaran)
            ->get();

    	$title = 'Rekapitulasi Pengeluaran dan Pemasukan Koperasi IT Del';
    	
    	return view('admin.laporan.rekapitulasi',['laporan'=>$laporan],compact('title'));
    }

    public function CetakPdf()
    {
        $pengeluaran = DB::table('t_pengeluaran')
        ->select(
            "t_pengeluaran.keterangan",
            "t_pengeluaran.tanggal",
            "t_pengeluaran.jumlah",
            "t_pengeluaran.jenis"
        );

        $laporan = DB::table('t_pemasukan')
        ->select(
            "t_pemasukan.keterangan",
            "t_pemasukan.tanggal",
            "t_pemasukan.jumlah",
            "t_pemasukan.jenis"            
            )
        ->union($pengeluaran)
        ->get();
        
        $title="Rekapitulasi";
        $pengeluaran = DB::table('t_pengeluaran')->get();
        $pdf = PDF::loadview('admin.laporan.rekapitulasi_pdf',['laporan'=>$laporan],compact('title'));
        return $pdf->stream();
    }

    public function CariLaporan(Request $request)
    {
        $title = 'Rekapitulasi Pengeluaran dan Pemasukan Koperasi IT Del';

        //menangkap data pencarian
        $cari = $request->cari;

        //mengambil data rekening sesuai pencarian
        $pengeluaran = DB::table('t_pengeluaran')->whereMonth('tanggal',$cari)
            ->select(
                "t_pengeluaran.keterangan",
                "t_pengeluaran.tanggal",
                "t_pengeluaran.jumlah",
                "t_pengeluaran.jenis"
            );

        $laporan = DB::table('t_pemasukan') ->whereMonth('tanggal',$cari)
            ->select(
                "t_pemasukan.keterangan",
                "t_pemasukan.tanggal",
                "t_pemasukan.jumlah",
                "t_pemasukan.jenis"            
                )
            ->union($pengeluaran)
            ->get();

    	$title = 'Rekapitulasi Pengeluaran dan Pemasukan Koperasi IT Del';
    	
    	$pdf = PDF::loadview('admin.laporan.rekapitulasi_pdf',['laporan'=>$laporan],compact('title'));
        return $pdf->stream();
    }
       
}