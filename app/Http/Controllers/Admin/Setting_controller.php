<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;

class Setting_controller extends Controller
{
    public function index(){
    	$title = 'Setting';

    	return view('admin.setting.setting_index',compact('title'));
    }

    public function ChangePassword(Request $request){
        $username = $request->username;
        $email = $request->email;
        $password = $request->password;
        $new_password = $request->new_password; //Hash::make($request['new_password']);
        $password_confirmation = $request->password_confirmation; //Hash::make($request['password_confirmation']);
    
        if($new_password == $password_confirmation){
            $new_password = Hash::make($request['new_password']);
        } else {
            Session::flash('gagal','Password confirmation anda salah !');
            return redirect('/admin/setting');
        }

        $users = DB::table('t_user')->where(['username'=> $username])->first();
    
        if($users->username == $username AND $users->email_user == $email AND  Hash::check($password, $users->password)){
            DB::table('t_user')->where(['username'=>$username])->update([
                'password'=>$new_password
            ]);

            Session::flash('sukses','Password berhasil diubah!');
            return redirect('/admin/setting');            
        }else{
            return redirect('/admin/setting');
        }
    
    }
}
