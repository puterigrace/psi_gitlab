<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Session;

class Pelanggan_controller extends Controller
{
    public function index()
    {   
        //mengambil data pelanggan dari database
        $pelanggan = DB::table('t_pelanggan')->get();
        
        //mengirim data pelanggan ke view
        $title='Daftar Pelanggan Koperasi IT Del';
        return view('admin.pelanggan.pelanggan',['pelanggan'=>$pelanggan],compact('title'));    
    }

    public function TambahPelanggan()
    {
        $title="Tambah Pelanggan";
        //view untuk manambah
        return view('admin.pelanggan.tambah_pelanggan',compact('title'));       
    }

    public function SimpanPelanggan(Request $request)
    {
        $this->validate($request,
        [
            'id_pelanggan'=>'required',
            'email'=>'required',
            'status'=>'required',
            'username_pelanggan' => 'required',
            'id_rekening' => 'required'
        ]);                 
        
        $id_rekening = $request->id_rekening;
        $idrekening = DB::table('t_rekening')->where(['id_rekening'=> $id_rekening])->value('id_rekening');
       
        $pelanggan = DB::table('t_pelanggan')->where(['id_rekening'=> $id_rekening])->value('id_pelanggan');
        
        if($pelanggan==""){

        if($id_rekening == $idrekening){

        //insert data pelanggan baru
        DB::table('t_pelanggan')->insert([
            'id_pelanggan' => $request->id_pelanggan,
            'email' => $request->email,
            'status' => $request->status,
            'username_pelanggan' => $request->username_pelanggan,
            'id_rekening' => $id_rekening
        ]);
        Session::flash('sukses','Tambah data pelanggan sukses !');
        return redirect('/admin/pelanggan');

        }else{
            Session::flash('failed','ID Rekening tidak ditemukan !');
            return redirect('/admin/pelanggan');
        }
    }else{
        Session::flash('failed','ID Rekening sudah dipakai !');
        return redirect('admin/pelanggan');
    }
    }

    public function UpdatePelanggan(Request $request)
    {
        DB::table('t_pelanggan')->where('id_pelanggan',$request->id)->update([
            'id_pelanggan' => $request->id,
            'email' => $request->email,
            'status' => $request->status,
            'username_pelanggan' => $request->username_pelanggan,
            'id_rekening' => $request->id_rekening
        ]);
        Session::flash('sukses','Data pelanggan telah di edit !');
        return redirect('/admin/pelanggan');
    }

    public function EditPelanggan($id)
    {
        //mengambil data berdasarkan id pelanggan
        $pelanggan = DB::table('t_pelanggan')->where('id_pelanggan',$id)->get();
        //passing data ke view
        $title="Edit Pelanggan";
        return view('admin.pelanggan.edit_pelanggan',['pelanggan'=>$pelanggan],compact('title'));
    }

    public function HapusPelanggan($id)
    {
        //mengambil data berdasarkan id pelanggan
        $pelanggan = DB::table('t_pelanggan')->where('id_pelanggan',$id)->delete();
        $id_rekening = DB::table('t_pelanggan')->where('id_pelanggan',$id)->value('id_rekening');
        DB::table('t_rekening')->where('id_rekening',$id_rekening)->delete();
        //alihkan halaman
        Session::flash('hapus','Data pelanggan berhasil di hapus !');
        return redirect('/admin/pelanggan');
    }

    public function CariPelanggan(Request $request)
    {
        $title='Daftar Pelanggan Koperasi IT Del';

        //menangkap data pencarian
        $cari = $request->cari;

        //mengambil data rekening sesuai pencarian
        $pelanggan = DB::table('t_pelanggan')
        ->where('username_pelanggan','like',"%".$cari."%")
        ->orWhere('id_pelanggan','like',"%".$cari."%")
        ->orWhere('id_rekening','like',"%".$cari."%")
        ->paginate();

        //passing data hasil pencarian
        return view('admin.pelanggan.pelanggan',['pelanggan'=>$pelanggan],compact('title'));  
    }
}