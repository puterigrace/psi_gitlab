<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Status_produk_controller extends Controller
{
    public function index(){
        $barang = DB::table('t_produk')->get();
        $title = 'Status Produk Koperasi IT Del';
    	// \Session::flash('pesan','Hai..');

    	return view('admin.barang.barang_status',['barang' => $barang],compact('title'));
    }
}
