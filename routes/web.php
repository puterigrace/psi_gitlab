<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Kasir Pos
Route::get('/','Auth\LoginController@index');
Route::get('/yajra','Beranda\Beranda_controller@yajra');
Route::get('/get/{id}','Beranda\Beranda_controller@get');
Route::post('/submit/{code}','Beranda\Beranda_controller@submit');
Route::get('/hapus-temp/{id}/{code}','Beranda\Beranda_controller@hapus_temp');
Route::post('/selesai/{code}/{total}','Beranda\Beranda_controller@selesai');
Route::get('/hapus-transaksi/{code}','Beranda\Beranda_controller@hapus_transaksi');

Route::post('/simpan-transaksi/{code}','Beranda\Beranda_controller@simpan_transaksi');

Route::get('/open-transaksi/{code}','Beranda\Beranda_controller@open_transaksi');

Route::get('/new-transaksi/{code}','Beranda\Beranda_controller@new_transaction');

// Admin ============================================================================================


	//Beranda
	Route::get('/admin', 'Admin\Beranda_controller@index');

	//User
	Route::get('/admin/user','Admin\User_controller@index');
	Route::get('/admin/user/aktivasi/{id}','Admin\User_controller@AktivasiUser');
	Route::get('/admin/user/hapus/{id}','Admin\User_controller@HapusUser');

	//Produk
	Route::get('/admin/produk','Admin\Produk_controller@index');
	Route::get('/admin/produk/yajra','Admin\Produk_controller@yajra');
	Route::post('/admin/produk','Admin\Produk_controller@store');
	Route::get('/admin/produk/get/{id}','Admin\Produk_controller@getData');
	Route::put('/admin/produk/{id}','Admin\Produk_controller@update');
	Route::get('/admin/produk/status','Admin\Status_produk_controller@index');

	//Login Pelanggan
	Route::get('/admin/pelanggan/login','Admin\Login_pelanggan_controller@index');
	Route::get('/admin/pelanggan/transaksi','Admin\Login_pelanggan_controller@AuthCheck');
	
	//Keluar Pelanggan
	Route::get('/admin/pelanggan/logout','Admin\Login_pelanggan_controller@logout');
	
	//Pelanggan
	Route::get('/admin/pelanggan','Admin\Pelanggan_controller@index');
	Route::get('/admin/pelanggan/tambah','Admin\Pelanggan_controller@TambahPelanggan');
	Route::post('/admin/pelanggan/store','Admin\Pelanggan_controller@SimpanPelanggan');
	Route::get('/admin/pelanggan/edit/{id}','Admin\Pelanggan_controller@EditPelanggan');
	Route::post('/admin/pelanggan/update','Admin\Pelanggan_controller@UpdatePelanggan');
	Route::get('/admin/pelanggan/hapus/{id}','Admin\Pelanggan_controller@HapusPelanggan');
	Route::get('/admin/pelanggan/cari','Admin\Pelanggan_controller@CariPelanggan');

	//Transaksi
	Route::get('/admin/transaksi','Admin\Transaksi_controller@index');
	Route::get('/admin/transaksi/yajra','Admin\Transaksi_controller@yajra');
	Route::get('/admin/transaksi/tanggal','Admin\Transaksi_controller@periksa');
	Route::get('/admin/transaksi/yajra/{tgl1}/{tgl2}','Admin\Transaksi_controller@yajra_tanggal');

	//Rekening
	Route::get('/admin/rekening','Admin\Rekening_controller@index');
	Route::get('/admin/rekening/tambah','Admin\Rekening_controller@TambahRekening');
	Route::post('/admin/rekening/store','Admin\Rekening_controller@SimpanRekening');
	Route::get('/admin/rekening/edit/{id}','Admin\Rekening_controller@EditRekening');
	Route::post('/admin/rekening/update','Admin\Rekening_controller@UpdateRekening');
	Route::get('/admin/rekening/hapus/{id}','Admin\Rekening_controller@HapusRekening');
	Route::get('/admin/rekening/info/{id}','Admin\Rekening_controller@InfoRekening');
	Route::post('/admin/rekening/inject','Admin\Rekening_controller@TambahSaldo');
	Route::get('/admin/rekening/cari','Admin\Rekening_controller@CariRekening');

	//Supplier
	Route::get('/admin/supplier','Admin\Supplier_controller@index');
	Route::get('/admin/supplier/tambah','Admin\Supplier_controller@TambahSupplier');
	Route::post('/admin/supplier/store','Admin\Supplier_controller@SimpanSupplier');
	Route::get('/admin/supplier/edit/{id}','Admin\Supplier_controller@EditSupplier');
	Route::post('/admin/supplier/update','Admin\Supplier_controller@UpdateSupplier');
	Route::get('/admin/supplier/hapus/{id}','Admin\Supplier_controller@HapusSupplier');

	//Laporan Pengeluaran
	Route::get('/admin/laporan','Admin\Laporan_controller@index');
	Route::get('/admin/laporan/tambah','Admin\Laporan_controller@TambahLaporan');
	Route::post('/admin/laporan/store','Admin\Laporan_controller@SimpanLaporan');
	Route::get('/admin/laporan/edit/{id}','Admin\Laporan_controller@EditLaporan');
	Route::post('/admin/laporan/update','Admin\Laporan_controller@UpdateLaporan');
	Route::get('/admin/laporan/hapus/{id}','Admin\Laporan_controller@HapusLaporan');
	Route::get('/admin/laporan/cetak_pdf','Admin\Laporan_controller@CetakPdf');

	//Laporan Pemasukan
	Route::get('/admin/lapor','Admin\Laporan_pemasukan_controller@index');
	Route::get('/admin/lapor/tambah','Admin\Laporan_pemasukan_controller@TambahLaporan');
	Route::post('/admin/lapor/store','Admin\Laporan_pemasukan_controller@SimpanLaporan');
	Route::get('/admin/lapor/edit/{id}','Admin\Laporan_pemasukan_controller@EditLaporan');
	Route::post('/admin/lapor/update','Admin\Laporan_pemasukan_controller@UpdateLaporan');
	Route::get('/admin/lapor/hapus/{id}','Admin\Laporan_pemasukan_controller@HapusLaporan');
	Route::get('/admin/lapor/cetak_pdf','Admin\Laporan_pemasukan_controller@CetakPdf');
	
	//Rekapitulasi
	Route::get('/admin/rekapitulasi','Admin\Rekapitulasi_controller@index');
	Route::get('/admin/rekapitulasi/cari','Admin\Rekapitulasi_controller@CariLaporan');
	Route::get('/admin/rekapitulasi/cetak_pdf','Admin\Rekapitulasi_controller@CetakPdf');
	
	//Setting
	Route::get('/admin/setting','Admin\Setting_controller@index');
	Route::post('/admin/setting/reset','Admin\Setting_controller@ChangePassword');

	//Keluar
	Route::get('/keluar','Beranda\Beranda_controller@keluar');

	//Register User
	Route::get('/register','Auth\RegisterController@index');
	Route::post('/register/user','Auth\RegisterController@RegisterUser');

	//Login User
	Route::get('/login','Auth\LoginController@index');
	Route::post('/login/user','Auth\LoginController@AuthCheck');
	
	Route::get('/home', function(){
	return redirect('admin');
});
